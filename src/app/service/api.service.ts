import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class ApiService {
  API_URL = environment?.apiurl;

  constructor(private http: HttpClient) {}

  httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    observe: 'response',
    reportProgress: true,
    responseType: 'json',
    withCredentials: false,
    mode: 'cors',
  };
  get(uri: string) {
    return this.http.get(this.API_URL + uri, this.httpOptions);
  }

  post(uri: string, payload: any) {
    return this.http.post(this.API_URL + uri, payload);
  }

  put(uri: string, payload: any) {
    return this.http.put(this.API_URL + uri, payload);
  }

  delete(uri: string) {
    return this.http.delete(this.API_URL + uri);
  }
}
