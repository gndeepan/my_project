import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { Token } from '@angular/compiler';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}
  currentuser: any = {};
  accessToken: any = {};

  login(payload: any): Observable<any> {
    return this.http.post(environment?.apiurl + 'user/login', payload).pipe(
      map((user: any) => {
        // console.log(user);

        // login successful if there's a jwt token in the response
        if (user && user.response.token.accessToken) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem(
            'accessToken',
            JSON.stringify(user.response.token.accessToken)
          );
        }

        return user;
      })
    );
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('accessToken');
  }

  getUserRole() {
    // Extract the user role from the access token and return it

    this.accessToken = JSON.parse(localStorage.getItem('currentUser') || '{}');

    return this.accessToken.response.user.u_role;
  }

  getCurrentuser() {
    this.currentuser = JSON.parse(localStorage.getItem('currentUser') || '{}');
    return this.currentuser;
  }

  isAuthorized(requiredRole: any) {
    // Check if the user has the required role
    // console.log(this.getUserRole());
    // console.log(requiredRole);

    return this.getUserRole() === requiredRole;
  }

  get isLoggedIn(): boolean {
    return !!localStorage.getItem('currentUser');
  }
}
