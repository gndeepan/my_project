import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { ToastService } from '../service/toast-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(
    private authService: AuthService,
    private router: Router,
    private toastService: ToastService
  ) {}

  ngOnInit(): void {
    this.form.reset();
  }

  showSuccess(text: any) {
    this.toastService.show(text, {
      classname: 'bg-success text-light',
      delay: 5000,
    });
  }
  showDanger(text: any) {
    this.toastService.show(text, {
      classname: 'bg-danger text-light',
      delay: 5000,
    });
  }

  onlogin() {
    console.log(this.form.value);
    this.login();
  }

  login() {
    this.authService.login(this.form.value).subscribe({
      next: (response) => {
        console.log(response);

        if (response.responseCode === 200) {
          // localStorage.setItem('accessToken', response.data.accessToken);
          console.log(this.authService.getUserRole());

          if (this.authService.getUserRole()) {
            this.router.navigate(['/main']);
          } else {
            this.showDanger('Something Problem! Please Try Again');
            this.router.navigate(['']);
          }
        }
      },
      error: (e) => {
        this.showDanger('Please check email and password');
      },
    });
  }
}
