import { HttpClient, HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../service/api.service';
import { AuthService } from '../service/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent {
  addUser!: FormGroup;
  dtOptions: DataTables.Settings = {};
  currentUser: any;
  persons!: any[];
  buttonvalue: any = 'Add New User';
  loading: boolean = false;

  constructor(
    config: NgbModalConfig,
    private modalService: NgbModal,
    private authenticationService: AuthService,
    private http: HttpClient,
    private fb: FormBuilder,
    private apiservice: ApiService
  ) {
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
    this.currentUser = this.authenticationService?.getCurrentuser();

    this.addUser = this.fb.group({
      u_id: [null, []],
      u_first_name: [null, [Validators.required]],
      u_last_name: [null, [Validators.required]],
      u_email: [null, [Validators.required]],
      u_role: [null, [Validators.required]],
      u_dob: [null, [Validators.required]],
      u_phone: [null, [Validators.required]],
    });
  }

  edituser(input: any) {
    this.addUser.patchValue(input);
    this.buttonvalue = 'Update User';
  }
  open(content: any) {
    this.modalService.open(content, { size: 'lg' });
  }

  onAddUser() {
    if (this.buttonvalue === 'Add New User') {
      this.loading = true;
      if (this.addUser.valid) {
        const payload = this.addUser.value;
        this.apiservice.post('user/signUp', payload).subscribe({
          next: (response: any) => {
            this.loading = false;
            console.log(response);
            this.persons.push(payload);
            this.modalService.dismissAll();
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: 'User Added Sucessfully',
              showConfirmButton: false,
              timer: 2000,
            });
          },
          error: (err: any) => {
            console.log(err);
            this.loading = false;
          },
        });
        this.addUser.reset();
      }
    } else {
      if (this.addUser.valid) {
        const payload = this.addUser.value;
        this.apiservice
          .put(`user/updateUser/${payload.u_id}`, payload)
          .subscribe({
            next: (response: any) => {
              this.loading = false;
              console.log(response);
              this.modalService.dismissAll();
              const foundSelectedUserIndex = this.persons.findIndex(
                (u: any) => u.u_id == payload.u_id
              );
              console.log(foundSelectedUserIndex);

              this.persons[foundSelectedUserIndex] = {
                ...this.persons[foundSelectedUserIndex],
                ...payload,
              };

              Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Updated User Sucessfully',
                showConfirmButton: false,
                timer: 2000,
              });
            },
            error: (err: any) => {
              console.log(err);
              this.loading = false;
            },
          });
        this.addUser.reset();
      }
    }
  }

  ngOnInit(): void {
    this.tabledataset();
  }

  tabledataset() {
    const that = this;

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      dom: '<"top"fr>rt<"bottom"lp><"clear">',
      serverSide: true,
      processing: true,
      lengthMenu: [5, 10, 20, 50],
      lengthChange: true,
      language: {
        searchPlaceholder: 'Search...',
      },
      ajax: (dataTablesParameters: any, callback) => {
        let queryParams = new HttpParams();
        queryParams = queryParams.append('skip', dataTablesParameters.start);
        queryParams = queryParams.append('limit', dataTablesParameters.length);
        queryParams = queryParams.append('page', dataTablesParameters.draw);
        queryParams = queryParams.append(
          'search',
          dataTablesParameters.search.value
        );
        queryParams = queryParams.append(
          'sortDir',
          dataTablesParameters.order[0].dir
        );

        console.log(queryParams);

        that.http
          .get(`http://localhost:4000/user`, { params: queryParams })
          .subscribe((resp: any) => {
            console.log(resp);
            console.log(dataTablesParameters);

            that.persons = resp?.response?.data;

            callback({
              recordsTotal: resp.response.count,
              recordsFiltered: resp.response.count,
              data: [],
            });
          });
      },
      columns: [
        { data: 'u_id' },
        { data: 'u_first_name' },
        { data: 'u_last_name' },
      ],
    };
  }
}
