import { Component, ViewChild } from '@angular/core';
import {
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart,
} from 'ng-apexcharts';
import { ApiService } from 'src/app/service/api.service';

// export type ChartOptions = {
//   series: ApexNonAxisChartSeries;
//   chart: ApexChart;
//   responsive: ApexResponsive[];
//   labels: any;
// };
@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.scss'],
})
export class PiechartComponent {
  @ViewChild('chart') chart?: PiechartComponent;

  public chartOptions: any;
  chartvalue: any = [];
  constructor(private apiService: ApiService) {
    this.chartOptions = {
      fill: {
        colors: ['#C3F1B5', '#E6B1B9'],
      },

      series: this.chartvalue,

      chart: {
        width: 580,
        type: 'pie',
      },
      labels: ['Success login count', 'Failed login count'],

      responsive: [
        {
          breakpoint: 480,
        },
      ],
    };
  }

  ngOnInit(): void {
    this.getuserDashboard();
  }

  getuserDashboard() {
    this.apiService.get('user/userDashboard').subscribe({
      next: (data: any) => {
        const user = data.body.response;
        console.log(user);

        this.chartvalue.push(user.successLoginCount, user.failedLoginCount);
      },
      error: (e) => {
        console.log(e);
      },
    });
  }
}
