import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from '../service/api.service';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CustomPaginator } from '../CustomPaginatorConfiguration';
import { PiechartComponent } from './piechart/piechart.component';

class DataTablesResponse {
  data!: any[];
  draw!: number;
  recordsFiltered!: number;
  recordsTotal!: number;
}
/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [
    { provide: MatPaginatorIntl, useValue: CustomPaginator() }, // Here
  ],
})
export class DashboardComponent {
  totalUser: any;
  totalActiveuser: any;
  totalDisableUser: any;
  totalDeletedUser: any;
  userlogsdata: any = [];
  getuserboard!: Subscription;
  displayedColumns: string[] = ['s_no', 'username', 'login_timings', 'status'];
  dataSource!: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngOnInit(): void {
    this.getuserDashboard();
    this.getuserDashboardLogs();
  }

  getuserDashboard() {
    this.getuserboard = this.apiService.get('user/userDashboard').subscribe({
      next: (data: any) => {
        const user = data.body.response;
        // console.log(data.body.response);
        this.totalUser = user.totalUser;
        this.totalActiveuser = user.activeUser;
        this.totalDisableUser = user.disabledUser;
        this.totalDeletedUser = user.deletedUser;
      },
      error: (e) => {
        console.log(e);
      },
    });
  }

  getuserDashboardLogs() {
    this.getuserboard = this.apiService.get('user/login-logs').subscribe({
      next: (data: any) => {
        const user = data.body.response;

        this.userlogsdata = user.data;
        console.log(this.userlogsdata);
        this.tabledata();
        // console.log(data.body.response);
      },
      error: (e) => {
        console.log(e);
      },
    });
  }

  constructor(private apiService: ApiService) {}

  async tabledata() {
    let users: any = [];
    await this.userlogsdata.map((data: any) => {
      users.push({
        s_no: data.l_id,
        username: this.getusername(data.l_user_name),
        login_timings: this.formatetime(data.l_created),
        status: data.l_login_status,
      });
    });
    console.log(users);

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  formatetime(input: any) {
    const date = new Date(input);

    const hours = date.getHours() % 12 || 12;
    const minutes = date.getMinutes();
    const ampm = date.getHours() >= 12 ? 'PM' : 'AM';
    const formattedTime = `${hours}:${minutes
      .toString()
      .padStart(2, '0')} ${ampm}`;
    return formattedTime;
  }

  getusername(input: any) {
    let name = input.split('@')[0];
    return name;
  }
}

export interface UserData {
  s_no: string;
  username: any;
  login_timings: string;
  status: string;
}
