import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthService
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    // const currentUser = this.authuser.getuser;
    // if (currentUser) {
    //   // check if route is restricted by role
    //   if (
    //     route.data['roles'] &&
    //     route.data['roles'].indexOf(currentUser.data.user_name) === -1
    //   ) {
    //     // role not authorised so redirect to home page
    //     this.router.navigate(['/user']);
    //     return false;
    //   }
    //   // authorised so return true
    //   return true;
    // }
    // // not logged in so redirect to login page with the return url
    // this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    // return true;
    const requiredRole = route.data['rolls'];
    console.log(requiredRole);
    if (this.authenticationService.isLoggedIn) {
      if (
        requiredRole &&
        requiredRole.indexOf(this.authenticationService.getUserRole()) === -1
      ) {
        this.router.navigate(['/']);
        return false;
      }

      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
