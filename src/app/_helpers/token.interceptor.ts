import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    // add auth header with jwt if user is logged in and request is to api url
    const currentUser = this.authenticationService?.getCurrentuser();

    const isLoggedIn = currentUser && currentUser?.response?.token;

    const isApiUrl = request?.url?.startsWith(environment?.apiurl);
    console.log(isLoggedIn);
    console.log(currentUser);
    console.log(isApiUrl);
    if (isLoggedIn && isApiUrl) {
      request = request.clone({
        setHeaders: {
          Authorization: `jwt ${currentUser?.response?.token?.accessToken}`,
        },
      });
    }

    return next.handle(request);
  }
}
