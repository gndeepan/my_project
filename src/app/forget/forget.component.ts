import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-forget',
  templateUrl: './forget.component.html',
  styleUrls: ['./forget.component.scss']
})
export class ForgetComponent {
  user_name = '';
  password = '';

  form = new FormGroup({
    user_name: new FormControl('', Validators.required),
    password: new FormControl('', [Validators.required]),
  });
  constructor(public authService: AuthService, private router: Router) {}

  login() {
    this.authService.login(this.form.value).subscribe((response) => {
      if (response.status === 200) {
        // localStorage.setItem('accessToken', response.data.accessToken);
        console.log(this.authService.getUserRole());

        if (this.authService.getUserRole() === 'user') {
          this.router.navigate(['/user']);
        } else {
          this.router.navigate(['/home']);
        }
      }
    });
  }

  logout() {
    this.authService.logout();
  }
}
