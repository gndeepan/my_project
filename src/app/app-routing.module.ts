import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForgetComponent } from './forget/forget.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { MapComponent } from './map/map.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserComponent } from './user/user.component';
import { AuthGuard } from './_helpers/auth.guard';
import { Roll } from './_helpers/roll';

const routes: Routes = [
  {
    path: 'main',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    data: { rolls: [Roll.admin, Roll.user] },
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'forget',
    component: ForgetComponent,
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    data: { rolls: [Roll.admin, Roll.user] },
  },
  {
    path: 'map',
    component: MapComponent,
    canActivate: [AuthGuard],
    data: { rolls: [Roll.admin, Roll.user] },
  },
  {
    path: 'user',
    component: UserComponent,
    canActivate: [AuthGuard],
    data: { rolls: [Roll.admin] },
  },
  {
    path: 'user-profile',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
    data: { rolls: [Roll.admin, Roll.user] },
  },
  // { path: 'home', component: AppComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
