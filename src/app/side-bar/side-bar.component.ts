import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
})
export class SideBarComponent {
  constructor(public authservice: AuthService, private router: Router) {}
  logout() {
    Swal.fire({
      title: 'Are you sure want to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes logout',
    }).then((result) => {
      if (result.value) {
        this.authservice.logout();
        this.router.navigate(['']);
        // Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
      }
    });
  }
}
