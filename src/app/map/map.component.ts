import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import Map from 'ol/Map.js';
import View from 'ol/View.js';
import FullScreen from 'ol/control/FullScreen';
import olms from 'ol-mapbox-style';
import { transform } from 'ol/proj';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import { ApiService } from '../service/api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import OSM from 'ol/source/OSM.js';
import TileLayer from 'ol/layer/Tile.js';
import { Circle, Fill, Stroke, Text } from 'ol/style.js';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements AfterViewInit {
  @ViewChild('map')
  private mapContainer!: ElementRef<HTMLElement>;
  userlatlng: any = [];
  CountryCount: any = {};
  country: any = 'Country';
  countryArray: any = [];
  vectorLayer: any;
  vectorSource: any;
  initialState = {
    lng: 78.9629,
    lat: 20.5937,
    zoom: 4,
  };
  maps!: Map;
  marker: any = [];
  markerStyle: any;

  myAPIKey = 'b3b1b471d3944ce7a4e04e35c8ff4792';
  mapStyle = 'https://maps.geoapify.com/v1/styles/osm-liberty/style.json';

  constructor(private apiservice: ApiService, private modalService: NgbModal) {}

  ngOnInit(): void {
    this.getCountry();
    this.getcity(this.country);
    this.getCountryCount(this.country);
  }

  getCountry() {
    this.apiservice.get(`user/getCountry`).subscribe({
      next: async (e: any) => {
        // console.log(e?.body?.response);
        this.countryArray = await e?.body?.response;
      },
    });
  }

  open(content: any) {
    this.modalService.open(content);
  }
  selectcountry(input: any) {
    this.country = input;
    this.getCountryCount(input);
    this.getcity(input);
  }

  async getcity(input: any) {
    this.apiservice.get(`user/getCity/${input}`).subscribe({
      next: async (e: any) => {
        // console.log(e?.body?.response);
        this.userlatlng = await e?.body?.response;
        this.vectorLayer = {};

        this.mapset(this.userlatlng);
        this.intialmap();
      },
    });
  }

  async getCountryCount(input: any) {
    this.apiservice.get(`user/getCountryCount/${input}`).subscribe({
      next: (e: any) => {
        // console.log(e?.body?.response);
        this.CountryCount = e?.body?.response;
      },
    });
  }

  ngAfterViewInit() {
    this.setmarker();
  }

  async setmarker() {
    olms(
      this.mapContainer.nativeElement,
      `${this.mapStyle}?apiKey=${this.myAPIKey}`
    ).then((map: Map | any) => {
      this.maps = map;

      this.mapset(this.userlatlng);
      this.intialmap();
    });
  }

  mapset(userlatlng: []) {
    this.marker = [];
    this.vectorSource?.clear();

    setTimeout(() => {
      userlatlng.forEach((latlang: any, index) => {
        this.marker.push(
          new Feature({
            geometry: new Point(
              transform(
                [latlang.c_lng, latlang.c_lat],
                'EPSG:4326',
                'EPSG:3857'
              )
            ),
            name: latlang.c_city,
          })
        );
        //

        this.markerStyle = new Style({
          image: new Icon({
            src: 'assets/svg/marker.svg',
            imgSize: [40, 40],
          }),
          // text: new Text({
          //   text: latlang.c_city,
          // }),
        });

        this.marker[index].setStyle(this.markerStyle);
      });

      this.vectorSource = new VectorSource({
        features: this.marker,
      });

      this.vectorLayer = new VectorLayer({
        source: this.vectorSource,
      });
      this.maps?.removeLayer(this.vectorLayer);

      this.maps?.addLayer(this.vectorLayer);
    }, 500);
  }

  intialmap() {
    this.maps?.setView(
      new View({
        center: transform(
          [
            this.userlatlng[0]?.c_lng ?? this.initialState.lng,
            this.userlatlng[0]?.c_lat ?? this.initialState.lat,
          ],
          'EPSG:4326',
          'EPSG:3857'
        ),
        zoom: this.initialState.zoom,
      })
    );

    this.maps?.addControl(new FullScreen());
  }
}
