import { Component } from '@angular/core';
import Swal from 'sweetalert2';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { ApiService } from '../service/api.service';
import { Router } from '@angular/router';
import { ToastService } from '../service/toast-service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent {
  userdata: any;
  public updatePassword!: FormGroup;
  // closeResult!: string;

  constructor(
    config: NgbModalConfig,
    private router: Router,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private api: ApiService,
    private toastService: ToastService
  ) {
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;

    this.updatePassword = this.fb.group(
      {
        old_password: [null, [Validators.required]],
        new_password: [null, [Validators.required]],
        confmpassword: [null, [Validators.required]],
      },
      { validators: this.checkPasswords }
    );

    this.updatePassword.reset();
  }

  checkPasswords(group: FormGroup) {
    let newPassword = group.controls['new_password'].value;
    let confmpassword = group.controls['confmpassword'].value;

    return newPassword === confmpassword ? null : { notSame: true };
  }

  showDanger(text: any) {
    this.toastService.show(text, {
      classname: 'bg-danger text-light',
      delay: 5000,
    });
  }

  open(content: any) {
    this.modalService.open(content);
    this.updatePassword.reset();
  }
  onUpdate() {
    if (this.updatePassword.valid) {
      const payload = this.updatePassword.value;
      this.api.post('auth/changePassword', payload).subscribe({
        next: (response: any) => {
          console.log(response);
          this.modalService.dismissAll();
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Password  Updated Sucessfully',
            showConfirmButton: false,
            timer: 2000,
          });
        },
        error: (err: any) => {
          console.log(err);
          this.showDanger('Somthing Problem! Please Try Again');
        },
      });
      this.updatePassword.reset();
    }
  }

  dateofBirth(input: any) {
    const timestamp = '2001-05-25T18:30:00.000Z';
    const dateOfBirth = new Date(timestamp);
    const formattedDate = dateOfBirth.toLocaleDateString('en-GB', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
    });
    return formattedDate;
  }

  ngOnInit(): void {
    const currentUser = JSON.parse(localStorage.getItem('currentUser') || '');
    this.userdata = currentUser.response.user;
    console.log('userdata', this.userdata);
  }
}
